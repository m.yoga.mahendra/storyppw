# import datetime
# import time

# from django.test import Client, LiveServerTestCase, TestCase
# from django.urls import resolve
# from django.utils import timezone

# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver.common.keys import Keys

# from .models import Message
# from .views import delete, story

# # Django Test Cases
# class StoryTest(TestCase):
#     def test_story_url_exists(self):
#         response = Client().get('/')
#         self.assertEqual(response.status_code, 200)

#     def test_using_story_view(self):
#         found = resolve('/') 
#         self.assertEqual(found.func, story)

#     def test_using_story_template(self):
#         response = Client().get('/')
#         self.assertTemplateUsed(response, 'index.html')
        
#     def test_form_exists(self):
#         response = Client().get('/')
#         self.assertIn('</form>', response.content.decode())
        
#     def test_add_remove_messages(self):
#         Message.objects.create(name="Test", location="Kampus", messages="The message")
#         test = Message.objects.filter(name="Test")
#         self.assertTrue(test)
#         test.delete()
#         test = Message.objects.filter(name="Test")
#         self.assertFalse(test)
        
#     def test_views_post(self):
#         response = Client().post('/',{"name":"Test", "location":"Kampus", "messages":"The message"})
#         response = Client().get('/')
#         self.assertIn("Test", response.content.decode())
#         self.assertIn("Kampus", response.content.decode())
#         self.assertIn("The message", response.content.decode())
#         response = Client().post('/delete',{'id': Message.objects.all().first().id})
#         print(response.status_code)
#         response = Client().get('/')
#         self.assertNotIn("Test", response.content.decode())
#         self.assertNotIn("Kampus", response.content.decode())
#         self.assertNotIn("The message", response.content.decode())
        
        
# # Selenium Test Cases    
# class MessageTestSelenium(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument("--disable-dev-shm-usage")
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')        
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
#         super(MessageTestSelenium, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(MessageTestSelenium, self).tearDown()

#     def test_upload_and_delete_story(self):
#         browser = self.browser
#         # Opening the link we want to test
#         browser.get('http://127.0.0.1:8000/')
#         # find the form element
#         time.sleep(3)
#         name = browser.find_element_by_id('id_name')
#         location = browser.find_element_by_id('id_location')
#         messages = browser.find_element_by_id('id_messages')
#         submit = browser.find_element_by_id('id_submit')
#         # Fill the form with data
#         name.send_keys('Yoga Mahendra')
#         location.send_keys('Kampus')
#         messages.send_keys('Hai, saya lagi belajar nih :D')
#         # submitting the form
#         submit.click()
#         time.sleep(3)
#         # check the returned result
#         assert 'Yoga Mahendra' in browser.page_source
#         assert 'Kampus' in browser.page_source
#         assert 'Hai, saya lagi belajar nih :D' in browser.page_source
#         # deletes the uploaded story
#         delete = browser.find_elements_by_class_name('button-story')
#         delete[0].click()
#         assert 'Yoga Mahendra' not in browser.page_source
#         assert 'Kampus' not in browser.page_source
#         assert 'Hai, saya lagi belajar nih :D' not in browser.page_source
#         time.sleep(3)