from django.db import models
from datetime import datetime
from django.utils import timezone
from django.core.validators import MinLengthValidator

# Create your models here.
class Message(models.Model):
    name = models.CharField(max_length=50,default='Yoga Mahendra')
    location = models.CharField(max_length=50,blank=True)
    messages = models.CharField(max_length=255) 
    datetime = models.DateTimeField(default=datetime.now,blank=True)
