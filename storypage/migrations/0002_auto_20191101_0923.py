# Generated by Django 2.1.1 on 2019-11-01 02:23

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('storypage', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='message',
            name='email',
        ),
        migrations.AddField(
            model_name='message',
            name='datetime',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='message',
            name='location',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='message',
            name='messages',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='message',
            name='name',
            field=models.CharField(default='Yoga Mahendra', max_length=50),
        ),
        migrations.AlterField(
            model_name='message',
            name='subject',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
