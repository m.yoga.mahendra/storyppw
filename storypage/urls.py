from django.urls import path
from . import views

urlpatterns = [
    path('', views.story, name='storypage'),
    path('delete', views.delete),
]