from django.urls import path

from . import views

urlpatterns = [
    path('', views.bookpage, name='bookpage'),
]