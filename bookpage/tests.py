import time

from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from .views import bookpage


# Django Test
class IndexTest(TestCase):
    def test_index_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_view(self):
        found = resolve('/')
        self.assertEqual(found.func, bookpage)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'bookpage.html')

# Selenium Test
class IndexLiveTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(IndexLiveTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(IndexLiveTest, self).tearDown()

    def test_check_title(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(3)
        self.assertEqual(selenium.title, 'Story TDD Yoga')
        
    def test_query_submit(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(1)
        search = selenium.find_element_by_id('searchbox')
        search.send_keys('Python')
        search.send_keys(Keys.RETURN)
        time.sleep(5)
        tabel = selenium.find_element_by_css_selector('table')
        inner_table = selenium.execute_script('return arguments[0].innerText;', tabel)
        self.assertIn('Python', inner_table)
        
