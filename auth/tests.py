import time

from django.contrib.auth.models import User
from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from .views import login


class loginTest(TestCase):
    def test_login_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_login_view(self):
        found = resolve('/')
        self.assertEqual(found.func, login)

    def test_using_login_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'login.html')
        
    def test_logout_url_exists(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)
        
    def test_hello_url_exists(self):
        response = Client().get('/hello/')
        self.assertEqual(response.status_code, 302)

    def test_wrong_username_message(self):
        response = Client().get('/?failed')
        self.assertIn('Wrong username/password', response.content.decode())

    def test_need_login_message(self):
        response = Client().get('/?reqlogin')
        self.assertIn('You need to log in first', response.content.decode())

    def test_logged_out_message(self):
        response = Client().get('/?loggedout')
        self.assertIn('Logout successful', response.content.decode())

    def test_login_logout(self):
        client = Client()
        username = 'yoga'
        password = 'yoga123'
        User.objects.create_user(username=username, password=password)
        # Login
        response = client.post('/login/', {
            'username': username,
            'password': password
        })
        # Test if login successful
        response = client.get('/hello/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Selamat datang, yoga.', response.content.decode())
        # Logout
        response = client.get('/logout/')
        # Test if logout successful
        response = client.get('/hello/')
        self.assertEqual(response.status_code, 302)

class loginLiveTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(loginLiveTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(loginLiveTest, self).tearDown()

    def test_check_title(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(3)
        self.assertEqual(selenium.title, 'Story TDD Yoga')

    def test_bypass_login(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/hello/')
        time.sleep(3)
        self.assertIn('You need to log in first', selenium.page_source)
    
    def test_wrong_login(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(3)
        elemUsername = selenium.find_element_by_id('inputUsername')
        elemUsername.send_keys('tes')
        elemPassword = selenium.find_element_by_id('inputPassword')
        elemPassword.send_keys('tes123')
        submitBtn = selenium.find_element_by_css_selector('button[type=submit]')
        submitBtn.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn('Wrong username/password', selenium.page_source)

    def test_login(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(3)
        elemUsername = selenium.find_element_by_id('inputUsername')
        elemUsername.send_keys('pepew')
        elemPassword = selenium.find_element_by_id('inputPassword')
        elemPassword.send_keys('pepew123')
        submitBtn = selenium.find_element_by_css_selector('button[type=submit]')
        submitBtn.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn('Selamat datang, Kak Pewe Keren', selenium.page_source)
        logoutBtn = selenium.find_element_by_css_selector('button[type=button]')
        logoutBtn.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn('Logout successful', selenium.page_source)
