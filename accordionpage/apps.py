from django.apps import AppConfig


class AccordionpageConfig(AppConfig):
    name = 'accordionpage'
